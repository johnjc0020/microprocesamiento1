#include <xc.h>

#pragma config FOSC = HS 
#pragma config WDTE = OFF 
#pragma config PWRTE = OFF 
#pragma config MCLRE = ON
#pragma config CP = OFF 
#pragma config CPD = OFF 
#pragma config BOREN = OFF 
#pragma config IESO = OFF
#pragma config FCMEN = OFF
#pragma config LVP = OFF 
#pragma config DEBUG = OFF 

#define _XTAL_FREQ 20000000

const unsigned char DIGITOS[10] = {0x3F, 0x06,  0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F};

int main(void)
{
    unsigned char i = 0;
    
    PORTC = 0x00;
    TRISC = 0x00;

    while(1)
    {
        PORTC = DIGITOS[i];
        i++;
        __delay_ms(1000);
    }
    
    return 0;
    
}