#include <xc.h>

#pragma config FOSC = HS 
#pragma config WDTE = OFF 
#pragma config PWRTE = OFF 
#pragma config MCLRE = ON
#pragma config CP = OFF 
#pragma config CPD = OFF 
#pragma config BOREN = OFF 
#pragma config IESO = OFF
#pragma config FCMEN = OFF
#pragma config LVP = OFF 
#pragma config DEBUG = OFF 

#define _XTAL_FREQ 20000000


int main(void)
{
    PORTA = 0x00;
    ANSEL = 0x00;
    TRISA = 0x0C;
    
    while(1)
    {
        if(RA2 == 0)
        {
            __delay_ms(100);
            
            if(RA2 == 0)
            {
                PORTA = 0x03;
            }
        }
        
        if(RA3 == 0)
        {
            __delay_ms(100);
            
            if(RA3 == 0)
            {
                PORTA = 0x30;
            }
        }
    }  

    return 0;
}
