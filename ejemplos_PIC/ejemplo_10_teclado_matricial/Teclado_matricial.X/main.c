#include <xc.h>

#pragma config FOSC = HS 
#pragma config WDTE = OFF 
#pragma config PWRTE = OFF 
#pragma config MCLRE = ON
#pragma config CP = OFF 
#pragma config CPD = OFF 
#pragma config BOREN = OFF 
#pragma config IESO = OFF
#pragma config FCMEN = OFF
#pragma config LVP = OFF 
#pragma config DEBUG = OFF 

#define _XTAL_FREQ 20000000

int leer_teclado(void)
{
    if(PORTB != 0x0F)
    {
        PORTB = 0XE0;
        if(RB3 == 0)
            return 1;
        if(RB2 == 0)
            return 2;
        if(RB1 == 0)
            return 3;
        if(RB0 == 0)
            return 4;
        
        PORTB = 0xD0;
        if(RB3 == 0)
            return 5;
        if(RB2 == 0)
            return 6;
        if(RB1 == 0)
            return 7;
        if(RB0 == 0)
            return 8;
        
        PORTB = 0xB0;
        if(RB3 == 0)
            return 9;
        if(RB2 == 0)
            return 10;
        if(RB1 == 0)
            return 11;
        if(RB0 == 0)
            return 12;
        
        PORTB = 0x70;
        if(RB3 == 0)
            return 13;
        if(RB2 == 0)
            return 14;
        if(RB1 == 0)
            return 15;
        if(RB0 == 0)
            return 16;
    }
    
    return 0;
}

int main(void)
{
    int teclado = 0;
    
    ANSELH = 0x00;
    PORTB = 0x00;    
    TRISB = 0x0F;
    WPUB = 0x0F;
    OPTION_REG = 0x7F;
    IOCB = 0x00;
    
    PORTC = 0x00;
    TRISC = 0x00;
    
    while(1)
    {
       teclado = leer_teclado();
       
       switch(teclado)
       {
           case 1:
               PORTC = 1;
               break;
           case 2:
               PORTC = 2;
               break;
           case 3:
               PORTC = 3;
               break;
           case 4:
               PORTC = 4;
               break;
           case 5:
               PORTC = 5;
               break;
           case 6:
               PORTC = 6;
               break;
           case 7:
               PORTC = 7;
               break;
           case 8:
               PORTC = 8;
               break;
           case 9:
               PORTC = 9;
               break;
           case 10:
               PORTC = 10;
               break;
           case 11:
               PORTC = 11;
               break;
           case 12:
               PORTC = 12;
               break;
           case 13:
               PORTC = 13;
               break;
           case 14:
               PORTC = 14;
               break;
           case 15:
               PORTC = 15;
               break;
           case 16:
               PORTC = 16;
               break;
       }
    }

    return 0;
}
