#include <xc.h>

#pragma config FOSC = HS 
#pragma config WDTE = OFF 
#pragma config PWRTE = OFF 
#pragma config MCLRE = ON
#pragma config CP = OFF 
#pragma config CPD = OFF 
#pragma config BOREN = OFF 
#pragma config IESO = OFF
#pragma config FCMEN = OFF
#pragma config LVP = OFF 
#pragma config DEBUG = OFF 

#define _XTAL_FREQ 20000000


void Inicializar_ADC()
{
    ADCON0 = 0x81; //Encender ADC y seleccionar el reloj: Fosc/32.
    ADCON1 = 0X00; //Referencia en Vss y Vdd, Justificado a la izqueirda.
}

void bits_seleccion(unsigned char d, unsigned char c, unsigned char b, unsigned char a)
{
    CHS3 = d;
    CHS2 = c;
    CHS1 = b;
    CHS0 = a;
}

void seleccionar_canal(unsigned char canal)
{
    bits_seleccion(0,0,0,0);
    
    switch(canal)
    {
        case 0:
            bits_seleccion(0,0,0,0);
            break;
        case 1:
            bits_seleccion(0,0,0,1);
            break;
        case 2:
            bits_seleccion(0,0,1,0);
            break;
        case 3:
            bits_seleccion(0,0,1,1);
            break;
        case 4:
            bits_seleccion(0,1,0,0);
            break;
        case 5:
            bits_seleccion(0,1,0,1);
            break;
        case 6:
            bits_seleccion(0,1,1,0);
            break;
        case 7:
            bits_seleccion(0,1,1,1);
            break;
        case 8:
            bits_seleccion(1,0,0,0);
            break;
        case 9:
            bits_seleccion(1,0,0,1);
            break;
        case 10:
            bits_seleccion(1,0,1,0);
            break;
        case 11:
            bits_seleccion(1,0,1,1);
            break;
        case 12:
            bits_seleccion(1,1,0,0);
            break;
        case 13:
            bits_seleccion(1,1,0,1);
            break;
        default:
            bits_seleccion(0,0,0,0);
            break;
    }
}

unsigned int Leer_ADC(unsigned char canal)
{  
    seleccionar_canal(canal);
    __delay_ms(2);      //Tiempo de adquisici�n.
    GO_nDONE = 1;       //Inicialozar la conersi�n ADC.
    while(GO_nDONE);    //Esperar a que la conversi�n se complete.
    
    return ((ADRESH<<8) + ADRESL); //Retornar resultado.
}

int main(void)
{
    unsigned int dato_adc = 0;
    
    PORTE  = 0x00; //Limpiar el puerto E.
    TRISE  = 0x01; //RA0 como entrada.
    ANSEL  = 0x01; //RA0 como entrada anal�gica.
    
    PORTC = 0x00;
    TRISC = 0x00;
    
    PORTD = 0x00;
    TRISD = 0x00;
    
    Inicializar_ADC(); 
    
    while(1)
    {
        dato_adc = Leer_ADC(5); //Leer el canal 5 del ADC.
        
        PORTC = dato_adc;
        PORTD = dato_adc>>8;
        __delay_ms(100);
    }
    
    return 0;
}
