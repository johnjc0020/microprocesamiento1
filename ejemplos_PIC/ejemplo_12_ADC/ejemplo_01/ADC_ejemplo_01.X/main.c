#include <xc.h>

#pragma config FOSC = HS 
#pragma config WDTE = OFF 
#pragma config PWRTE = OFF 
#pragma config MCLRE = ON
#pragma config CP = OFF 
#pragma config CPD = OFF 
#pragma config BOREN = OFF 
#pragma config IESO = OFF
#pragma config FCMEN = OFF
#pragma config LVP = OFF 
#pragma config DEBUG = OFF 

#define _XTAL_FREQ 20000000


void Inicializar_ADC()
{
    ADCON0 = 0x81; //Encender ADC y seleccionar el reloj: Fosc/32.
    ADCON1 = 0X00; //Referencia en Vss y Vdd, Justificado a la izqueirda.
}

unsigned int Leer_ADC(unsigned char canal)
{
    if(canal > 13)
        return 0;
    
    ADCON0 &= 0xC5;     //Limpiar la selecci�n de bits.
    ADCON0 |= canal<<2; //Se establecen los bits CHS3, CHS2, CHS1 y CHS0.
    __delay_ms(2);      //Tiempo de adquisici�n.
    GO_nDONE = 1;       //Inicialozar la conersi�n ADC.
    while(GO_nDONE);    //Esperar a que la conversi�n se complete.
    
    return ((ADRESH<<8) + ADRESL); //Retornar resultado.
}

int main(void)
{
    unsigned int dato_adc = 0;
    
    PORTA  = 0x00; //Limpiar el puerto A.
    TRISA  = 0x01; //RA0 como entrada.
    ANSEL  = 0x01; //RA0 como entrada anal�gica.
    
    PORTC = 0x00;
    TRISC = 0x00;
    
    PORTD = 0x00;
    TRISD = 0x00;
    
    Inicializar_ADC(); 
    
    while(1)
    {
        dato_adc = Leer_ADC(0); //Leer el canal 0 del ADC.
        
        PORTC = dato_adc;
        PORTD = dato_adc>>8;
        __delay_ms(100);
    }
    
    return 0;
}