#include<stdio.h>
#include<math.h>


float fahrenheit2celsius(float temperatura)
{
    float fahrenheit = 0.0;
    float celsius = 0.0;

    fahrenheit = temperatura;

    celsius = (5.0/9.0)*(fahrenheit - 32.0);

    return celsius;
}


float celsius2fahrenheit(float temperatura)
{
    float fahrenheit = 0.0;
    float celsius = 0.0;

    celsius = temperatura;

    fahrenheit = (9.0/5.0)*celsius + 32.0;

    return fahrenheit;
}


int main(void)
{
    float temperatura;
    char unidad;

    float resultado;

    printf("Ingrese las unidades de temperatura:\n");
    printf("F para convertir de °F a °C\n");
    printf("C para convertir de °C a °F\n");
    scanf("%c", &unidad);


    printf("Ingrese el valor de temperatura:\n");
    scanf("%f", &temperatura);


    switch(unidad)
    {
        case 'F':
            resultado = fahrenheit2celsius(temperatura);
            printf("%f°F = %f°C\n", temperatura, resultado);
            break;
        case 'C':
            resultado = celsius2fahrenheit(temperatura);
            printf("%f°C = %f°F\n", temperatura, resultado);
            break;
        default:
            printf("Elección no válida.\n");
            break;
    }


    return 0;
}
