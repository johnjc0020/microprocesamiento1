#include <stdio.h>


int funcion(double *suma, double *producto, double x, double y)
{
    *suma = x + y;
    *producto = x*y;

    return 0;
}


int main(void)
{
    double x = 0.0;
    double y = 0.0;

    double suma = 0.0;
    double producto = 0.0;


    printf("\nIngrese dos números: \n");
    scanf("%lf", &x);
    scanf("%lf", &y);


    funcion(&suma, &producto, x, y);


    printf("\nLa suma de %lf y %lf es %lf.\n", x, y, suma);
    printf("\nEl producto de %lf y %lf es %lf.\n\n", x, y, producto);

    return 0;
}
