#include <stdio.h>
#include <math.h>


int main(void)
{
    unsigned int i = 0;

    double x = 0.0;
    double step = 0.5;

    double senx = 0.0;
    double cosx = 0.0;
    double tanx = 0.0;
    double expx = 0.0;


    for(i=0; i<10; i++)
    {
        senx = sin(x);
        cosx = cos(x);
        tanx = tan(x);
        expx = exp(x);

        printf("Cuando x = %f, sen(x) = %f, cos(x) = %f, tan(x) = %f y exp(x) = %f\n", x, senx, cosx, tanx, expx);

        x += step;
    }


    return 0;
}
