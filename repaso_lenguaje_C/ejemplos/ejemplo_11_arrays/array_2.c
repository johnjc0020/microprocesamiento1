#include <stdio.h>

int main(void)
{
    int i = 0, j = 0;

    int array1[2] = {1, 10};
    double array2[2][3] = {{6.4, -3.1, 55.0}, {63.0, -100.9, 50.8}};


    printf("\nImprimir array 1:\n");

    for(i=0; i<2; i++)
    {
        printf("array1[%d] = %d\n", i, array1[i]);
    }


    printf("\nImprimir array 2:\n");

    for(i=0; i<2; i++)
    {
        for(j=0; j<3; j++)
        {
            printf("array2[%d][%d] = %f\n", i, j, array2[i][j]);
        }
    }


    return 0;
}
