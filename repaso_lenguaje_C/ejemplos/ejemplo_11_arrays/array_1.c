#include <stdio.h>

int main(void)
{
    int i = 0, j = 0;

    int array1[2];
    double array2[2][3];


    array1[0] = 1;
    array1[1] = 10;


    array2[0][0] = 6.4;
    array2[0][1] = -3.1;
    array2[0][2] = 55.0;
    array2[1][0] = 63.0;
    array2[1][1] = -100.9;
    array2[1][2] = 50.8;


    printf("\nImprimir array 1:\n");

    for(i=0; i<2; i++)
    {
        printf("array1[%d] = %d\n", i, array1[i]);
    }


    printf("\nImprimir array 2:\n");

    for(i=0; i<2; i++)
    {
        for(j=0; j<3; j++)
        {
            printf("array2[%d][%d] = %f\n", i, j, array2[i][j]);
        }
    }


    return 0;
}
