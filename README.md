# Material de apoyo para Microprocesamiento I
- Blog PIC: https://diegorestrepoleal.blogspot.com/2021/11/microcontroladores-pic16f887.html
- Repaso lenguaje C: https://diegorestrepoleal.blogspot.com/2021/11/repaso-del-lenguaje-c.html
- Guía rápida de nano y tmux: https://diegorestrepoleal.blogspot.com/2022/02/guia-rapida-de-nano-y-tmux.html

# Cómo Grabar un PIC con PICkit 3
- Blog: https://minicontroller.blogspot.com/2021/07/como-grabar-un-pic-con-pickit-3.html
- Vídeo: https://www.youtube.com/watch?v=mkGMmyKLE38

# Data Sheet del PIC16F887
- Documento: http://ww1.microchip.com/downloads/en/devicedoc/41291d.pdf

# Materiales:
- Lista de materiales: https://gitlab.com/diegorestrepo/microprocesamiento1/-/blob/master/Documentos/materiales.pdf
